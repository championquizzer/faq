# Frequenty Asked Questions

This is a repository accumulating questions from users of Tor from various platforms like the official Blog, Reddit, Twitter, Tor Stack Exchange, etc.

# Legend
Every question will be accompanied with a checkbox, a tick against the question means that it has been answered. An url with the question and the answer is provided against every entry.

# Questions 

- [x] [Can Tor really hide my location when I access websites that has the tracing function?](https://tor.stackexchange.com/questions/20775/can-tor-really-hide-my-location-when-i-access-websites-that-has-the-tracing-func/21627#21627) -[answer](https://tor.stackexchange.com/a/21627/31025)
- [x] [How to use Tails/Tor with sites that need Javascript](https://tor.stackexchange.com/questions/21555/how-to-use-tails-tor-with-sites-that-need-javascript/21626#21626) -[answer](https://tor.stackexchange.com/a/21626/31025) 
- [ ] [More FAQ questions - Issue 142](https://gitlab.torproject.org/tpo/web/community/-/issues/142) 
- [ ] [Launch Tor I get: “The Tor executable is missing"](https://tor.stackexchange.com/questions/12662/launch-tor-i-get-the-tor-executable-is-missing)
- [ ] [What are the differences between Tails and Whonix?](https://tor.stackexchange.com/questions/1814/what-are-the-differences-between-tails-and-whonix)

# TO-DO
- [ ] Breakdown of the question into categories like easy, medium, difficult and/or technical, non-technical, conceptual
- [ ] Sorting in order of frequency (number of times the question is asked)
- [ ] Breakdown on basis of websites
- [ ] Adding necessary and frequently used resources (like blog posts, or links from the official manual), so that whomever wants to answer has some material handy.

# **Internship Timeline**

Things to accomplish:

a) Update user documentation.  

a.1) I would help keep the documentation updated especially during the releases.

a.2) We have some very useful information in our old docs (e.g- https://trac.torproject.org/projects/tor/wiki/doc/TorPlusVPN) which I would like to migrate to our new documentation pages. 

b) Answer Tor users on frontdesk, IRC, blog posts and other social forums.   

b.1) I would be actively answering user queries, taking help from the community. (Timeline: Entire duration of internship)

b.2) I would accumulate questions (that I don't have adequate knowledge to answer) from various blog posts, Reddit threads, Twitter discussions and provide them to our teams on various IRC channels like #tor-www, #tor-dev, #tor-project so that other community members can answer them. **[Proposal]** We can also hold IRC meetings on a fortnightly basis by using some scheduling tool like https://whenisgood.net/ (note: this website uses Google Analytics, we should look for more privacy-respecting service if we can) for more pressing questions or discussions. (Timeline: Entire duration of internship)   

b.3) **[Proposal]** Our friends at LibreOffice run a Tip of the Day on Twitter (e.g.- https://twitter.com/libreoffice/status/1320681601634865152), which seems quite interesting to me. I was wondering if we can do something like that, say weekly, to answer some recurring user questions or just interesting tips on using Tor Browser to keep our social media community engaged. I am willing to work on this after consultations with our social media team and the community (Timeline: Subject to discussion, but weekly as proposed).

c) Collect user feedback about Tor and Tor Browser on monthly basis and make a report. I would base my observations mostly from reviews and comments on blog posts, app stores, etc. Take community feedback on how we can improve on that end.(Timeline: Monthly)

d) Weekly report/meeting with my mentors regarding my work. (Timeline: Weekly)

e) **[Proposal]** I would like to see increased presence of the Tor project at FLOSS events (or even other events like Linux fests). I would be willing to present talks or organize introductory workshops on how one can get started with our project both as an user or a contributor. 
 
